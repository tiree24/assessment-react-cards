import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './components/card/Card.css';
import  './components/cardGroup/CardGroup.css';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
